import '@babel/polyfill'
import '../src/main.css'
window.addEventListener('DOMContentLoaded', () => {
 async function initial() {
   const temp = document.querySelector('#weather-temp')
   const time = document.querySelector('#weather-localetime')
   const res = await fetch(`http://api.weatherstack.com/current?access_key=6189b1d89b72b11fe35bdee12c32a8c7&query=Bangkok`)
   const response = await res.json()
   getBackground(response.current.weather_descriptions, response.location.region)
   temp.innerHTML = `${response.current.temperature}˚`
   time.innerHTML = `Local Time: ${response.location.localtime}`
 }
 initial()
 document.querySelector('#form').addEventListener('submit', async (e) => {
   e.preventDefault()
   const name = document.querySelector('#name').value
   const temp = document.querySelector('#weather-temp')
   const time = document.querySelector('#weather-localetime')
   const res = await fetch(`http://api.weatherstack.com/current?access_key=6189b1d89b72b11fe35bdee12c32a8c7&query=${name}`)
   const response = await res.json()
   getBackground(response.current.weather_descriptions, response.location.country)
   temp.innerHTML = `${response.current.temperature}˚`
   time.innerHTML = `Local Time: ${response.location.localtime}`
 })
 function getBackground(text, name) {
   let num = Math.floor((Math.random() * 2) + 1)
   let image
   if(text[0].match('rain')) {
     image = `rain_${num}.jpg`
   } else if(text[0].match('cloudy')) {
     image = `cloudy_${num}.jpg`
   } else if(text[0].match('cold')) {
     image = `cold_${num}.jpg`
   } else if(text[0].match('Sunny')) {
     image = `hot_${num}.jpg`
   } else if(text[0].match('fine')) {
     image = `fine_${num}.jpg`
   }
   const country = document.querySelector('#country')
   country.innerHTML = name
   const background = document.querySelector('#weather-background')
   background.style.background = `linear-gradient(0deg,rgba(0, 0, 0, 0.3),rgba(0, 0, 0, 0.3)), url(./assets/images/${image}) center`
   background.style.backgroundSize = 'cover'
 }
})